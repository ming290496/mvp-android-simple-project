package sumedcoding.com.Model;

public interface IUser {
    String getEmail();
    String getPassword();
    String isValidData();
}
