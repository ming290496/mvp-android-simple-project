package sumedcoding.com.Model;

import android.text.TextUtils;
import android.util.Patterns;

public class User implements IUser {

    private String email, password;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String isValidData() {
        if (TextUtils.isEmpty(getEmail()))
            return "Email must be field";
        else if (!Patterns.EMAIL_ADDRESS.matcher(getEmail()).matches())
            return "Email must valid";
        else if (getPassword().length() < 6)
            return "Password length can not less than 6 characters";
        else {
            return "Success";
        }
    }
}
