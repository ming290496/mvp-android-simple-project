package sumedcoding.com.Presenter;

import sumedcoding.com.Model.User;
import sumedcoding.com.View.ILoginView;

public class LoginPresenter implements ILoginPresenter {

    ILoginView loginView;

    public LoginPresenter(ILoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void onLogin(String email, String password) {
        User user = new User(email, password);

        loginView.onLoginResult(user.isValidData());
    }
}
