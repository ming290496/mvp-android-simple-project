package sumedcoding.com.Presenter;

public interface ILoginPresenter {
    void onLogin(String email, String password);
}
